const Dotenv = require('dotenv-webpack');

module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],

  publicPath: process.env.NODE_ENV === 'production' ? '' : '/',

  configureWebpack: {
    plugins: [
      new Dotenv()
    ]
  },
}