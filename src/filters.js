import moment from "moment";

const TimeFormate12h = function(value) {
  return moment(value).format("DD/MM/yyyy, h:mm a");
};

const WhatsappuserPhone = function(value) {
  value = value.toString();
  var phone = value.split("@")[0];
  var firstDigits = phone.substring(0, 2);
  var lastDigits = phone.substring(3, 13);

  return `(${firstDigits}) ${lastDigits}`;
};

export { TimeFormate12h, WhatsappuserPhone };
